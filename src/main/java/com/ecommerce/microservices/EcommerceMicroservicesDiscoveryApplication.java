package com.ecommerce.microservices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class EcommerceMicroservicesDiscoveryApplication {

	public static void main(String[] args) {
		SpringApplication.run(EcommerceMicroservicesDiscoveryApplication.class, args);
	}

}

