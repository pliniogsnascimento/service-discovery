FROM maven:3.6.0-jdk-8-alpine AS builder
WORKDIR /home/app
COPY . /home/app
RUN mvn package -Dmaven.test.skip=true 
RUN ls /home/app/target

FROM openjdk:8-jre-alpine
WORKDIR /home/app
COPY --from=builder /home/app/target/EcommerceMicroservicesDiscovery-0.0.1-SNAPSHOT.jar /home/app/
ENTRYPOINT [ "java","-jar","EcommerceMicroservicesDiscovery-0.0.1-SNAPSHOT.jar" ] 
